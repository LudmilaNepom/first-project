name := """play-java-seed"""
organization := "com.example"
version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

//enablePlugins(ScalaJSPlugin, WorkbenchPlugin)


scalaVersion := "2.11.11"

// This is an application with a main method
//scalaJSUseMainModuleInitializer := true


libraryDependencies ++= Seq(
  javaJpa,
  filters,
  "mysql" % "mysql-connector-java" % "5.1.36",
  "org.eclipse.persistence" % "eclipselink" % "2.4.2",
  "org.webjars" %% "webjars-play" % "2.5.0",
  "org.webjars" % "bootstrap" % "3.1.1-2",
  "org.webjars.bower" % "jquery" % "3.2.1",
  "com.adrianhurt" % "play-bootstrap_2.11" % "1.0-P25-B3"
)
//  "org.scala-js" %%% "scalajs-dom" % "0.9.1",
//  "com.lihaoyi" %%% "scalatags" % "0.6.1"


