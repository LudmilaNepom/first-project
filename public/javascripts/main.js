$('#toTop').click(function () {
    $("html, body").animate({scrollTop: 0}, 600);
    return false;
});

$(document).ready(function () {

    $(document).on("click", '.ajaxPageUpdate', function (event) {

        var url = $(this).attr('href');


        $("#updateable").load(url);

        event.preventDefault();
        event.stopPropagation();
    });

});

$(document).ready(function () {

    $(document).on("click", '.navbar-right .lang', function (event) {
        var url = $(this).attr('href');

        $.get(url);
        event.preventDefault();
        event.stopPropagation();
        location.href = location.href;
    });

});

$(document).ready(function () {

    $(document).on("submit", '#workWithJson', function (event) {
        event.preventDefault();
        event.stopPropagation();
        var $form = $("#workWithJson");
        var url = $form.attr('action');
        var id = $form.find("input[name='id']").val();
        var firstName = $form.find("input[name='firstName']").val();
        var surname = $form.find("input[name='surname']").val();
        var email = $form.find("input[name='email']").val();
        var departments = $('#departments').val();
        var formData = {
             "id": id,
             "firstName": firstName,
             "surname": surname,
             "email": email,
             "departments": departments
        };
        $.ajax({
            type: 'POST',
            url: url,
            data: formData,
            error: function(jqXHR, response) {
                var errors = jQuery.parseJSON(jqXHR.responseText);
                console.log(jqXHR.responseText);
                console.log(errors);
                $(".form-group.has-error").hide().find('span').empty();
                $(errors).each( function( key, value ) {
                    $.each(value ,function(k, v){
                        console.log(k+" : "+ v);
                        var name="input[name="+k+"]";
                        $form.find(name).after('<div class="form-group has-error"><span class="help-block">' + v + '</span></div>');
                });



                });


            }
        });


    });
});




