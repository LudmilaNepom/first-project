package controllers;

import com.google.inject.Inject;
import models.entities.Department;
import models.entities.Person;
import models.repository.department.DepartmentRepository;
import models.repository.person.PersonRepository;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.Transactional;
import play.i18n.MessagesApi;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.ArrayList;
import java.util.List;

public class PersonController extends Controller {
    private final PersonRepository personRepository;
    private final DepartmentRepository departmentRepository;
    private final FormFactory formFactory;
    private MessagesApi messagesApi;
    private int itemsPerPage = 5;

    @Inject
    public PersonController(PersonRepository personRepository, FormFactory formFactory, MessagesApi messagesApi,
                            DepartmentRepository departmentRepository) {
        this.personRepository = personRepository;
        this.formFactory = formFactory;
        this.messagesApi = messagesApi;
        this.departmentRepository = departmentRepository;
    }

    @Transactional
    public Result list(int page) {
        int lastPage = personRepository.getCountOfPages(itemsPerPage);
        List<Person> persons = personRepository.getPage(page, itemsPerPage);
        return ok(views.html.person.list.render(persons, page, lastPage, itemsPerPage));
    }

    @Transactional
    public Result listPage(int page) {
        int lastPage = personRepository.getCountOfPages(itemsPerPage);
        List<Person> persons = personRepository.getPage(page, itemsPerPage);
        return ok(views.html.person.listpage.render(persons, page, lastPage, itemsPerPage));
    }

    @Transactional
    public Result listSearchResult() {
        DynamicForm requestData = formFactory.form().bindFromRequest();
        String search = requestData.get("search");
        List<Person> persons = new ArrayList<>();
        persons.addAll(personRepository.find(search));
        int lastNumber;
        if (itemsPerPage > persons.size()) {
            lastNumber = persons.size();
        } else {
            lastNumber = itemsPerPage;
        }
        List<Person> personsForPage = persons.subList(0, lastNumber);
        int lastPage = getCountOfSearchPages(itemsPerPage, persons);
        return ok(views.html.person.listsearchresult.render(personsForPage, 1, lastPage, search, itemsPerPage));
    }

    @Transactional
    public Result listPageSearchResult(int page, String search) {
        List<Person> persons = new ArrayList<>();
        persons.addAll(personRepository.find(search));
        int lastNumber;
        if ((page * itemsPerPage + 1) > persons.size()) {
            lastNumber = persons.size();
        } else {
            lastNumber = page * itemsPerPage;
        }
        List<Person> personsForPage = persons.subList((page - 1) * itemsPerPage, lastNumber);
        int lastPage = getCountOfSearchPages(itemsPerPage, persons);
        return ok(views.html.person.listpagesearchresult.render(personsForPage, page, lastPage, search, itemsPerPage));
    }

    @Transactional
    public Result add() {
        Form<Person> personForm = formFactory.form(Person.class);
        List<Department> departments = departmentRepository.getAll();
        return ok(views.html.person.edit.render(null, personForm, departments));
    }

    @Transactional
    public Result doAdd() {
        Form<Person> form = formFactory.form(Person.class).bindFromRequest();
        if (form.hasErrors()) {
            flash("error", messagesApi.get(lang(), "msg.form.error"));
            List<Department> departmentsAll = departmentRepository.getAll();
            return badRequest(views.html.person.edit.render(null, form, departmentsAll));
        }
        Person person = form.get();
        personRepository.save(person);
        flash("success", messagesApi.get(lang(), "msg.person.add.success"));
        return redirect(routes.PersonController.list(1));
    }

    @Transactional
    public Result edit(Long id) {
        Person person = personRepository.find(id);
        List<Department> departments = departmentRepository.getAll();
        Form<Person> personForm = formFactory.form(Person.class).fill(person);
        return ok(views.html.person.edit.render(person, personForm, departments));

    }

    @Transactional
    public Result doEdit(Long id) {
        Person person = personRepository.find(id);
        List<Department> departments = departmentRepository.getAll();

        Form<Person> form = formFactory.form(Person.class).fill(person).bindFromRequest();
        if (form.hasErrors()) {
            flash("error", messagesApi.get(lang(), "msg.form.error"));
            return badRequest(views.html.person.edit.render(person, form, departments));
        }

        Person editPerson = form.get();
        editPerson.setId(person.getId());
        editPerson.setRegistrationDate(person.getRegistrationDate());

        for (Department department : departments) {
            if (!editPerson.getDepartments().contains(department)) {
                department.getPersons().remove(person);
            }

            if (editPerson.getDepartments().contains(department)
                    && !department.getPersons().contains(editPerson)) {
                department.getPersons().add(person);
            }
            departmentRepository.save(department);
        }

        personRepository.save(editPerson);
        flash("success", messagesApi.get(lang(), "msg.person.edit.success"));
        return redirect(routes.PersonController.list(1));
    }

    @Transactional
    public Result delete(Long id) {
        Person person = personRepository.find(id);
        personRepository.delete(id);
        flash("success", messagesApi.get(lang(), "msg.person.delete.success"));
        return redirect(routes.PersonController.list(1));
    }

    private int getCountOfSearchPages(int itemsPerPage, List<Person> persons) {
        return ((persons.size()) / itemsPerPage) + 1;
    }


}
