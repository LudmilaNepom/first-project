package models.repository.person;

import models.entities.Person;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

public class JPAPersonRepository implements PersonRepository {

    final private JPAApi jpaApi;

    @Inject
    public JPAPersonRepository(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    @Override
    public Person save(Person person) {
        EntityManager em = jpaApi.em();
        return em.merge(person);
    }

    @Override
    public Person find(Long id) {
        EntityManager em = jpaApi.em();
        TypedQuery<Person> namedQuery = em.createNamedQuery("Person.findById", Person.class).setParameter("id", id);
        return namedQuery.getSingleResult();
    }

    @Override
    public List<Person> find(String search) {
        EntityManager em = jpaApi.em();
        List<Person> resultList = new ArrayList<>();
        Long id=new Long(-1);
        try {
            id=Long.parseLong(search);
        } catch(NumberFormatException e){}
        TypedQuery<Person> namedQueryById = em.createNamedQuery("Person.findBySearchString", Person.class)
                .setParameter("search", "%" + search + "%")
                .setParameter("id", id);
        namedQueryById.getResultList();
        return namedQueryById.getResultList();
    }

    @Override
    public void delete(Long id) {
        EntityManager em = jpaApi.em();
        TypedQuery<Person> namedQuery = em.createNamedQuery("Person.findById", Person.class).setParameter("id", id);
        em.remove(namedQuery.getSingleResult());
    }

    public List<Person> getAll() {
        EntityManager em = jpaApi.em();
        TypedQuery<Person> namedQuery = em.createNamedQuery("Person.getAll", Person.class);
        return namedQuery.getResultList();
    }

    public List<Person> getPage(int page, int itemsPerPage) {
        EntityManager em = jpaApi.em();
        TypedQuery<Person> namedQuery = em.createNamedQuery("Person.getAll", Person.class)
                .setFirstResult((page-1)*itemsPerPage)
                .setMaxResults(itemsPerPage);
        return namedQuery.getResultList();
    }

    public int getCountOfPages(int itemsPerPage){
        EntityManager em = jpaApi.em();
        Query queryTotal = em.createQuery("SELECT count (p.id) FROM Person p");
        long countResult = (long)queryTotal.getSingleResult();
        return ((int)(countResult/itemsPerPage)+1);
    }

}
