package models.repository.department;


import com.google.inject.ImplementedBy;
import models.entities.Department;

import java.util.List;

@ImplementedBy(JPADepartmentRepository.class)
public interface DepartmentRepository {
    Department save(Department department);

    Department find(Long id);

    void delete(Long id);

    List<Department> getAll();
}


