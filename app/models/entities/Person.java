package models.entities;

import play.data.format.Formats;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Entity
@Table(name = "insign_person")
@NamedQueries({
        @NamedQuery(name = "Person.getAll", query = "SELECT p FROM Person p"),
        @NamedQuery(name = "Person.findById", query = "SELECT p FROM Person p WHERE p.id=:id"),
        @NamedQuery(name = "Person.findBySearchString", query = "SELECT p " +
                "FROM Person p " +
                "WHERE" +
                " p.id = :id" +
                " OR p.firstName LIKE :search " +
                " OR p.surname LIKE :search")
})
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Constraints.Required
    @Constraints.MaxLength(value = 10)
    private String firstName;

    @Constraints.Required
    @Constraints.MaxLength(30)
    private String surname;

    @Constraints.Required
    @Constraints.Email(message = "msg.invalid.person.email")
    private String email;

    @Formats.DateTime(pattern = "yyyy-MM-dd HH:mm")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registrationDate;
    @ManyToMany
    private List<Department> departments=new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstname) {
        this.firstName = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public List<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(List<Department> departments) {
        this.departments = departments;
    }

    public void addDepartment (Department department){
        departments.add(department);
    }

    public void removeDepartment (Department department){
        departments.remove(department);
    }

    @PrePersist
    public void setCurrentDate() {
        Date date = new Date();
        this.setRegistrationDate(date);
    }


}
