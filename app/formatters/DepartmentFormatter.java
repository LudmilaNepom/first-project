package formatters;

import com.google.inject.Inject;
import models.entities.Department;
import models.repository.department.DepartmentRepository;
import play.data.format.Formatters.SimpleFormatter;

import java.text.ParseException;
import java.util.Locale;


public class DepartmentFormatter extends SimpleFormatter<Department> {
    private final DepartmentRepository departmentRepository;


    @Inject
    public DepartmentFormatter(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    @Override
    public Department parse(String input, Locale l) throws ParseException {
        return departmentRepository.find(Long.valueOf(input));
    }

    @Override
    public String print(Department department, Locale l) {
        return Long.toString(department.getId());
    }

}
